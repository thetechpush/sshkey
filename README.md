# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is for copy ssh key from master to all managed Linux servers.
* Version 1.0


### How do I get set up? ###

* Generate ssh key first on master/control node by command ssh-keygen.

### Contribution guidelines ###

* Modify the path of public key as per your location in playbook file.
* Now, run the playbook by --ask-pass (-k) option like "ansible-playbook sshkey.yml -k". It will prompt for ssh password.


### Who do I talk to? ###

* Repo owner "Indradeo Kumar"
* For any query, you can reach me on "inder.kumar1804@gmail.com"